package test.testIcon.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import test.testIcon.model.Fakultas;
import test.testIcon.model.Jurusan;
import test.testIcon.model.Mahasiswa;
import test.testIcon.model.MataKuliah;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataMahasiswa implements Serializable {
    private Long nim;
    private String nama;
    private Jurusan jurusan;
    private Fakultas fakultas;
    private MataKuliahDto mataKuliah;

}
