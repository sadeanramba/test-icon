package test.testIcon.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import test.testIcon.model.MataKuliah;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MataKuliahDto {
    private List<MataKuliah> mataKuliah;
}
