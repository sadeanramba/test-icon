package test.testIcon.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class DataMahasiswaResponse implements Serializable {
    private List<DataMahasiswa> dataMahasiswa;
    private int jumlah;
}
