package test.testIcon.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MahasiswaRequest implements Serializable {

    private long nim;
    private String nama;
    private String fakultas;
    private String jurusan;
    private String mataKuliah;
}
