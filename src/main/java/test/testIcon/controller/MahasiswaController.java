package test.testIcon.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import test.testIcon.dto.DataMahasiswa;
import test.testIcon.dto.DataMahasiswaResponse;
import test.testIcon.dto.MahasiswaRequest;
import test.testIcon.dto.MahasiswaResponse;
import test.testIcon.model.Fakultas;
import test.testIcon.model.Jurusan;
import test.testIcon.model.Mahasiswa;
import test.testIcon.repository.FakultasRepository;
import test.testIcon.repository.JurusanRepository;
import test.testIcon.service.MahasiswaService;

import javax.persistence.AssociationOverride;
import java.util.List;


@RestController
public class MahasiswaController {

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private JurusanRepository jurusanRepository;

    @Autowired
    private FakultasRepository fakultasRepository;

    //Untuk Save dan Update Mahasiswa
    @PostMapping("/addMahasiswaTest")
    public MahasiswaResponse addMahasiswa(@RequestBody MahasiswaRequest mahasiswaRequest) {
        return mahasiswaService.addMahasiswa(mahasiswaRequest);
    }

    @GetMapping("/getMahasiswa")
    public DataMahasiswaResponse getMahasiswa(){
        return mahasiswaService.getAllMahasiswa();
    }

    // Menampilkan mahasiswa yang mengambil mata kuliah
    @GetMapping("/getMahasiswaWithMatkul")
    public DataMahasiswaResponse getMahasiswaWithMatkul(){
        return mahasiswaService.getMahasiswaWithMatkul();
    }

    @PostMapping("/deleteMahasiswa")
    public MahasiswaResponse deleteMahasiswa(@RequestBody MahasiswaRequest mahasiswaRequest) {
        return mahasiswaService.deleteData(mahasiswaRequest);
    }

    // Data Mahasiswa pada salah 1 jurusan
    @GetMapping("/mahasiswainamajorupdate")
    public DataMahasiswaResponse mahasiwaInMajor(@RequestBody MahasiswaRequest mahasiswaRequest) {
        return mahasiswaService.getMahasiswaInMajor(mahasiswaRequest);
    }

}
