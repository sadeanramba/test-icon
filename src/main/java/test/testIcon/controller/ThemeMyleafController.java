package test.testIcon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import test.testIcon.dto.MahasiswaRequest;
import test.testIcon.model.Fakultas;
import test.testIcon.model.Jurusan;
import test.testIcon.model.Mahasiswa;
import test.testIcon.repository.FakultasRepository;
import test.testIcon.repository.JurusanRepository;
import test.testIcon.service.MahasiswaService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ThemeMyleafController {

    @Autowired
    private JurusanRepository jurusanRepository;

    @Autowired
    private FakultasRepository fakultasRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping("/registerMahasiswa")
    public String registerMahasiswa(Model model) {
        Mahasiswa mahasiswa = new Mahasiswa();
        //System.out.println("Fakultas"+model.getAttribute(mahasiswa.getFakultas()));
        model.addAttribute("mahasiswa", mahasiswa);

        List<String> listJurusan = new ArrayList<>();
        List<String> listFakultas = new ArrayList<>();
        List<Jurusan> listJurusanTemp= jurusanRepository.findAll();
        List<Fakultas> listFakultasTemp = fakultasRepository.findAll();
        for(Fakultas fakultas : listFakultasTemp){
            listFakultas.add(fakultas.getKodeFakultas());
        }
        for(Jurusan jurusan : listJurusanTemp){
            listJurusan.add(jurusan.getKodeJurusan());
        }
        //System.out.println("Fakultas : "+fakultasRepository.findAll());
        model.addAttribute("listJurusan", listJurusan);
        model.addAttribute("listFakultas", listFakultas);

        return "form_add_mahasiswa";
    }

    @PostMapping("/addMahasiswa")
    public String submitForm(@ModelAttribute("mahasiswa") MahasiswaRequest mahasiswa) {
        mahasiswaService.addMahasiswa(mahasiswa);
        return "Sukses";
    }
}
