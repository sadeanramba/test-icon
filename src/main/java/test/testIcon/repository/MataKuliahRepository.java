package test.testIcon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.testIcon.model.MataKuliah;

public interface MataKuliahRepository extends JpaRepository<MataKuliah, String> {

}
