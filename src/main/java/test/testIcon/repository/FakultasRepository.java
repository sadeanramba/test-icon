package test.testIcon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.testIcon.model.Fakultas;

public interface FakultasRepository extends JpaRepository<Fakultas, String> {
}
