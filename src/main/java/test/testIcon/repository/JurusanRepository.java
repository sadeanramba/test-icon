package test.testIcon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.testIcon.model.Jurusan;

public interface JurusanRepository extends JpaRepository<Jurusan,String> {
}
