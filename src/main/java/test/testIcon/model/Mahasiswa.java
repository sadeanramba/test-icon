package test.testIcon.model;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mahasiswa")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mahasiswa implements Serializable {

    @Id
    @NotNull
    @Column(name = "nim", length = 20)
    private long nim;

    @NotNull
    @Column(name = "nama_mahasiswa", length = 50)
    private String nama;

    @NotNull
    @Column(name = "jurusan", length = 20)
    private String jurusan;

    @NotNull
    @Column(name = "fakultas", length = 20)
    private String fakultas;

    @Column(name = "mata_kuliah", length = 20)
    private String mataKuliah;


}
