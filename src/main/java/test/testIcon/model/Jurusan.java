package test.testIcon.model;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "jurusan")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Jurusan implements Serializable {

    @Id
    @NotNull
    @Column(name = "kode_jurusan", length = 10)
    private String kodeJurusan;

    @NotNull
    @Column(name = "nama_jurusan", length = 50)
    private String namaJurusan;
}
