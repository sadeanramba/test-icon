package test.testIcon.model;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "mata_kuliah")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MataKuliah implements Serializable {

    @Id
    @NotNull
    @Column(name = "kode_mata_kuliah", length = 10)
    private String kodeMatkul;

    @NotNull
    @Column(name = "nama_mata_kuliah", length = 50)
    private String namaMatkul;

    @NotNull
    @Column(name = "jumlahsks_mata_kuliah", length = 50)
    private int jumlahSks;
}
