package test.testIcon.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.testIcon.dto.*;
import test.testIcon.model.Fakultas;
import test.testIcon.model.Jurusan;
import test.testIcon.model.Mahasiswa;
import test.testIcon.model.MataKuliah;
import test.testIcon.repository.FakultasRepository;
import test.testIcon.repository.JurusanRepository;
import test.testIcon.repository.MahasiswaRepository;
import test.testIcon.repository.MataKuliahRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class MahasiswaService {

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private JurusanRepository jurusanRepository;

    @Autowired
    private FakultasRepository fakultasRepository;

    public MahasiswaResponse addMahasiswa(MahasiswaRequest mahasiswaRequest){
        String status = "Mahasiswa berhasil Ditambahkan";
        //log.info("Mahasiswa Request : "+mahasiswaRequest);
        Mahasiswa mahasiswa = new Mahasiswa();

        Optional<Mahasiswa> checknim = mahasiswaRepository.findById(mahasiswaRequest.getNim());
        if(!checknim.isEmpty()){
            status = "Nim Sudah Digunakan";
        }else {
            mahasiswa.setNim(mahasiswaRequest.getNim());
            mahasiswa.setNama(mahasiswaRequest.getNama());
            mahasiswa.setFakultas(mahasiswaRequest.getFakultas());
            mahasiswa.setJurusan(mahasiswaRequest.getJurusan());
            mahasiswa.setMataKuliah(mahasiswaRequest.getMataKuliah());
            mahasiswaRepository.save(mahasiswa);
        }
        return MahasiswaResponse.builder().nim(mahasiswa.getNim()).nama(mahasiswa.getNama()).status(status).build();
    }

    public DataMahasiswaResponse getAllMahasiswa(){
        List<DataMahasiswa> listMahasiswa = new ArrayList<>();
        List<Mahasiswa> listDataMahasiswaTemp = mahasiswaRepository.findAll();
        for(Mahasiswa dataMahasiswa : listDataMahasiswaTemp){
            DataMahasiswa mahasiswa = new DataMahasiswa();
            mahasiswa.setNim(dataMahasiswa.getNim());
            mahasiswa.setNama(dataMahasiswa.getNama());
            Optional<Jurusan> jurusan = jurusanRepository.findById(dataMahasiswa.getJurusan());
            System.out.println("kode jurusan :"+dataMahasiswa.getJurusan());
            Optional<Fakultas> fakultas = fakultasRepository.findById(dataMahasiswa.getFakultas());
            mahasiswa.setJurusan(Jurusan.builder().kodeJurusan(jurusan.get().getKodeJurusan()).namaJurusan(jurusan.get().getNamaJurusan()).build());
            mahasiswa.setFakultas(Fakultas.builder().namaFakultas(fakultas.get().getNamaFakultas()).kodeFakultas(fakultas.get().getKodeFakultas()).build());
            List<MataKuliah> matkulList = new ArrayList<>();
            if(!dataMahasiswa.getMataKuliah().isEmpty()) {
                if (dataMahasiswa.getMataKuliah().contains(",")) {
                    String[] kodeMataKuliahList = dataMahasiswa.getMataKuliah().split(",");
                    for (String kodeMatkul : kodeMataKuliahList) {
                        MataKuliah mataKul = new MataKuliah();
                        Optional<MataKuliah> mataKuliah = mataKuliahRepository.findById(kodeMatkul);
                        //mataKul.seMataKuliah.builder().kodeMatkul(mataKuliah.get().getKodeMatkul()).namaMatkul(mataKuliah.get().getNamaMatkul()).jumlahSks(mataKuliah.get().getJumlahSks()).build()
                        mataKul.setKodeMatkul(mataKuliah.get().getKodeMatkul());
                        mataKul.setNamaMatkul(mataKuliah.get().getNamaMatkul());
                        mataKul.setJumlahSks(mataKuliah.get().getJumlahSks());
                        matkulList.add(mataKul);
                    }
                } else {
                    MataKuliah mataKul = new MataKuliah();
                    Optional<MataKuliah> mataKuliah = mataKuliahRepository.findById(dataMahasiswa.getMataKuliah());
                    //mataKul.seMataKuliah.builder().kodeMatkul(mataKuliah.get().getKodeMatkul()).namaMatkul(mataKuliah.get().getNamaMatkul()).jumlahSks(mataKuliah.get().getJumlahSks()).build()
                    mataKul.setKodeMatkul(mataKuliah.get().getKodeMatkul());
                    mataKul.setNamaMatkul(mataKuliah.get().getNamaMatkul());
                    mataKul.setJumlahSks(mataKuliah.get().getJumlahSks());
                    matkulList.add(mataKul);
                }
            }
            mahasiswa.setMataKuliah(MataKuliahDto.builder().mataKuliah(matkulList).build());
            listMahasiswa.add(mahasiswa);
        }
        return DataMahasiswaResponse.builder().dataMahasiswa(listMahasiswa).jumlah(listMahasiswa.size()).build();
    }

    public DataMahasiswaResponse getMahasiswaInMajor(MahasiswaRequest mahasiswaRequest){
        List<DataMahasiswa> listMahasiswa = new ArrayList<>();
        List<Mahasiswa> listDataMahasiswaTemp = mahasiswaRepository.findAll();
        for(Mahasiswa dataMahasiswa : listDataMahasiswaTemp) {
            if (dataMahasiswa.getJurusan().equalsIgnoreCase(mahasiswaRequest.getJurusan())) {
                DataMahasiswa mahasiswa = new DataMahasiswa();
                mahasiswa.setNim(dataMahasiswa.getNim());
                mahasiswa.setNama(dataMahasiswa.getNama());
                Optional<Jurusan> jurusan = jurusanRepository.findById(dataMahasiswa.getJurusan());
                System.out.println("kode jurusan :" + dataMahasiswa.getJurusan());
                Optional<Fakultas> fakultas = fakultasRepository.findById(dataMahasiswa.getFakultas());
                mahasiswa.setJurusan(Jurusan.builder().kodeJurusan(jurusan.get().getKodeJurusan()).namaJurusan(jurusan.get().getNamaJurusan()).build());
                mahasiswa.setFakultas(Fakultas.builder().namaFakultas(fakultas.get().getNamaFakultas()).kodeFakultas(fakultas.get().getKodeFakultas()).build());
                List<MataKuliah> matkulList = new ArrayList<>();
                if (!dataMahasiswa.getMataKuliah().isEmpty()) {
                    if (dataMahasiswa.getMataKuliah().contains(",")) {
                        String[] kodeMataKuliahList = dataMahasiswa.getMataKuliah().split(",");
                        for (String kodeMatkul : kodeMataKuliahList) {
                            MataKuliah mataKul = new MataKuliah();
                            Optional<MataKuliah> mataKuliah = mataKuliahRepository.findById(kodeMatkul);
                            //mataKul.seMataKuliah.builder().kodeMatkul(mataKuliah.get().getKodeMatkul()).namaMatkul(mataKuliah.get().getNamaMatkul()).jumlahSks(mataKuliah.get().getJumlahSks()).build()
                            mataKul.setKodeMatkul(mataKuliah.get().getKodeMatkul());
                            mataKul.setNamaMatkul(mataKuliah.get().getNamaMatkul());
                            mataKul.setJumlahSks(mataKuliah.get().getJumlahSks());
                            matkulList.add(mataKul);
                        }
                    } else {
                        MataKuliah mataKul = new MataKuliah();
                        Optional<MataKuliah> mataKuliah = mataKuliahRepository.findById(dataMahasiswa.getMataKuliah());
                        //mataKul.seMataKuliah.builder().kodeMatkul(mataKuliah.get().getKodeMatkul()).namaMatkul(mataKuliah.get().getNamaMatkul()).jumlahSks(mataKuliah.get().getJumlahSks()).build()
                        mataKul.setKodeMatkul(mataKuliah.get().getKodeMatkul());
                        mataKul.setNamaMatkul(mataKuliah.get().getNamaMatkul());
                        mataKul.setJumlahSks(mataKuliah.get().getJumlahSks());
                        matkulList.add(mataKul);
                    }
                }
                mahasiswa.setMataKuliah(MataKuliahDto.builder().mataKuliah(matkulList).build());
                listMahasiswa.add(mahasiswa);
            }
        }
        return DataMahasiswaResponse.builder().dataMahasiswa(listMahasiswa).jumlah(listMahasiswa.size()).build();
    }



    public DataMahasiswaResponse getMahasiswaWithMatkul(){
        List<DataMahasiswa> listMahasiswa = new ArrayList<>();
        List<Mahasiswa> listDataMahasiswaTemp = mahasiswaRepository.findAll();
        for(Mahasiswa dataMahasiswa : listDataMahasiswaTemp){
            if(!dataMahasiswa.getMataKuliah().isEmpty()) {
                DataMahasiswa mahasiswa = new DataMahasiswa();
                mahasiswa.setNim(dataMahasiswa.getNim());
                mahasiswa.setNama(dataMahasiswa.getNama());
                Optional<Jurusan> jurusan = jurusanRepository.findById(dataMahasiswa.getJurusan());
                System.out.println("kode jurusan :" + dataMahasiswa.getJurusan());
                Optional<Fakultas> fakultas = fakultasRepository.findById(dataMahasiswa.getFakultas());
                mahasiswa.setJurusan(Jurusan.builder().kodeJurusan(jurusan.get().getKodeJurusan()).namaJurusan(jurusan.get().getNamaJurusan()).build());
                mahasiswa.setFakultas(Fakultas.builder().namaFakultas(fakultas.get().getNamaFakultas()).kodeFakultas(fakultas.get().getKodeFakultas()).build());
                List<MataKuliah> matkulList = new ArrayList<>();
                if (dataMahasiswa.getMataKuliah().contains(",")) {
                    String[] kodeMataKuliahList = dataMahasiswa.getMataKuliah().split(",");
                    for (String kodeMatkul : kodeMataKuliahList) {
                        MataKuliah mataKul = new MataKuliah();
                        Optional<MataKuliah> mataKuliah = mataKuliahRepository.findById(kodeMatkul);
                        //mataKul.seMataKuliah.builder().kodeMatkul(mataKuliah.get().getKodeMatkul()).namaMatkul(mataKuliah.get().getNamaMatkul()).jumlahSks(mataKuliah.get().getJumlahSks()).build()
                        mataKul.setKodeMatkul(mataKuliah.get().getKodeMatkul());
                        mataKul.setNamaMatkul(mataKuliah.get().getNamaMatkul());
                        mataKul.setJumlahSks(mataKuliah.get().getJumlahSks());
                        matkulList.add(mataKul);
                    }
                } else {
                    MataKuliah mataKul = new MataKuliah();
                    Optional<MataKuliah> mataKuliah = mataKuliahRepository.findById(dataMahasiswa.getMataKuliah());
                    //mataKul.seMataKuliah.builder().kodeMatkul(mataKuliah.get().getKodeMatkul()).namaMatkul(mataKuliah.get().getNamaMatkul()).jumlahSks(mataKuliah.get().getJumlahSks()).build()
                    mataKul.setKodeMatkul(mataKuliah.get().getKodeMatkul());
                    mataKul.setNamaMatkul(mataKuliah.get().getNamaMatkul());
                    mataKul.setJumlahSks(mataKuliah.get().getJumlahSks());
                    matkulList.add(mataKul);
                }
                mahasiswa.setMataKuliah(MataKuliahDto.builder().mataKuliah(matkulList).build());
                listMahasiswa.add(mahasiswa);
            }
        }
        return DataMahasiswaResponse.builder().dataMahasiswa(listMahasiswa).jumlah(listMahasiswa.size()).build();
    }

    public MahasiswaResponse deleteData(MahasiswaRequest mahasiswa){
        mahasiswaRepository.deleteById(mahasiswa.getNim());
        return MahasiswaResponse.builder().nim(mahasiswa.getNim()).nama(mahasiswa.getNama()).status("Delete Sukses").build();
    }

}
