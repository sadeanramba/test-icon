package test.testIcon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class TestIconApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestIconApplication.class, args);
	}

}
